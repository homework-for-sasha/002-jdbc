import java.sql.*;

public class Main {
    public static final String DB_URL = "jdbc:postgresql://localhost:5432/homeworks";
    public static final String DRIVER = "org.postgresql.Driver";
    public static final String USER = "postgres";
    public static final String PASSWORD = "qwerty007";

    public static final String CREATE_TABLE = "CREATE TABLE homeworks (" +
            "id SERIAL PRIMARY KEY, " +
            "date DATE," +
            "student TEXT," +
            "link VARCHAR" +
            ");";
    public static final String INSERT_INTO_HOMEWORKS_HOMEWORK = "INSERT INTO homeworks (date, student, link) VALUES ('%s', '%s', '%s');";
    public static final String SELECT_HOMEWORK = "SELECT * FROM homeworks;";



    public static void main(String[] args) {
        Connection connection = null;
        Statement statement = null;

        try {
            Class.forName(DRIVER);
            connection = DriverManager.getConnection(DB_URL, USER, PASSWORD);
            statement = connection.createStatement();

            statement.executeUpdate(CREATE_TABLE);
            statement.executeUpdate(String.format(INSERT_INTO_HOMEWORKS_HOMEWORK, "01/06/2022", "tanuha", "https://gitlab.com/homework-_for-sasha/001-maven"));
            statement.executeUpdate(String.format(INSERT_INTO_HOMEWORKS_HOMEWORK, "01/06/2022", "tanuha", "https://gitlab.com/homework-for-sasha/002-jdbc"));
            ResultSet resultSet = statement.executeQuery(SELECT_HOMEWORK);
            while (resultSet.next()) {
                System.out.printf("id = %d || date = %s || student = %s || link = %s %n",
                        resultSet.getInt("id"),
                        resultSet.getString("date"),
                        resultSet.getString("student"),
                        resultSet.getString("link")
                );
            }


        }catch (SQLException e) {
            System.err.println(e.getMessage());
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            System.err.println(e.getMessage());
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                System.err.println(e.getMessage());;
            }
            try {
                statement.close();
            } catch (SQLException e) {
                System.err.println(e.getMessage());
            }
        }

    }




}
